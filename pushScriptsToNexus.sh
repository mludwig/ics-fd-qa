#!/bin/csh
# upload ics-fd-qa scripts to nexus so that they can be picked up from anywhere
# temporary location in venus
#
# authentication
set user=`whoami`
stty -echo 
echo -n "Enter password for user ** ${user} ** : "
set password = $<
stty echo

echo "==="
set ARCH="ics-fd-qa"
set NREPO="cern-venus"
set REPO="https://repository.cern.ch/nexus/content/repositories/"${NREPO}"/"${ARCH}

foreach fn ( `ls -1 scripts` )
   echo "uploading scripts/${fn} to nexus ${REPO}/${fn}"
   xterm -e "curl -v -u ${user}:${password} --upload-file ./scripts/${fn} ${REPO}/${fn} ";
end

