# Yvan's script to connect the sonar-scanner to CERN sonarQube
# execute once to set up the certificate
# the java certificate store will change with any update of new java version
# this is why we copy the certificate into ~/certificates
#
# 1 - Copy the certificate store <java_home>/jre/lib/security/cacerts
#     to a directory to import the sonar certificate only once
# 2 - Get the certificate from sonar server to a file (using openssl)
# 3 - Import the certificate into cacerts file with keytool
# 4 - Export the SONAR_SCANNER_OPTS variable containing the path to the certificate store 
echo "===executing script $0 START==="
XXX=$(readlink -f `which java` | sed "s/java$//g")
cd $XXX/../../jre/lib/security
pwd
ls -al
# cp cacerts ~/certificates
#cd /home/mludwig/certificates
cd ~/certificates
echo | openssl s_client -connect cvl-sonarqube.cern.ch:443 2>&1 | sed -n -e '/BEGIN\ CERTIFICATE/,/END\ CERTIFICATE/ p' > cvl-sonarqube.cern.ch.crt
# we can only import it if that alias sonar is not yet existing
keytool -import -file cvl-sonarqube.cern.ch.crt -alias sonar -keystore cacerts -storepass changeit
# sonar-scanner relies on this to find the trustStore, it seems it accepts only absolute paths
#export SONAR_SCANNER_OPTS="-Djavax.net.ssl.trustStore=/home/mludwig/certificates/cacerts"
export SONAR_SCANNER_OPTS="-Djavax.net.ssl.trustStore=~/certificates/cacerts"
echo "===executing script $0 END==="

