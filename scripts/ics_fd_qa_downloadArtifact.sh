#!/bin/bash
# download artifact from gitlab, by URL:
# i.e. URL="https://gitlab.cern.ch/mludwig/venusCaenSimulationEngine/-/jobs/5297929/artifacts/download"
URL=$1
# download token to access archive from the project, passed as payload
PROJECT_TOKEN=$2

#
ICS_FD_QA_EXIT_CODE_GITLAB_CURL_TOKEN=-1
ICS_FD_QA_EXIT_CODE_GITLAB_NOT_ZIP=-2 
# copied from riku's kobe sources, thanks ;-) should be integrated properly
RESULT="project_sources.zip"
echo "===executing script $0 START==="
echo "downloading from ${URL}"
echo "API token ${ICS_FD_API_TOKEN}"
echo "project token ${PROJECT_TOKEN}"

# if project token is defined we take it, otherwise the API token
if [ ! ${PROJECT_TOKEN} ]; then
	DLTOKEN=${ICS_FD_QA_READ}
	echo "no INPUT_PROJECT_TOKEN provided, using API token ICS_FD_QA_READ"
else
	DLTOKEN=${PROJECT_TOKEN}
	echo "a PROJECT_TOKEN was transmitted in the trigger payload and is used to download the source artifact"
fi

# keep it secret
echo "DLTOKEN is ${DLTOKEN}"

# if the dl fails, lets wait a bit and try again. The pipeline might have other stages after this call and
# might not be finished yet. Let's allow 1h before we abandon. Like this we should be fine using just a single
# call (client gitlab-ci.yml) for packing and triggering. 
failCount=100
until [ $failCount -lt 1 ]
do
  	echo "try download, countdown "$failCount
	if ! curl --fail -L -k --header "Private-Token: $DLTOKEN" "$URL" -o "$RESULT" ; then
  		echo "Curl failed with error code $? ! Sleep 60 and try again "$failCount
  		# keep it secret
 		# echo "${URL} ${DLTOKEN}"
  		((failCount=failCount-1))
  		sleep 10	
	else
	    # we downloaded something, but is it in the zip format as well?
	    echo "checking file type of "$RESULT
	    ls -l $RESULT
	    file $RESULT
	    file --brief --mime-type $RESULT
		if [ "$(file --brief --mime-type $RESULT)" != "application/zip" ]; then
	 	 	echo "Downloaded file '$RESULT' is not a zip archive! Try again "$failCount
	 	 	echo "===here is what we got==="
	 	 	cat $RESULT
	 	 	echo "==="
			((failCount=failCount-1))
			sleep 3
		else
	    	# we are OK
			failCount=-99
		fi
	fi
done
echo "...download ${failCount}"
if [ $failCount -eq -99 ]; then
	echo "succeeded download ${failCount}"
else
	echo "Curl failed with error code $? , event tried 10 times. Exiting..."
	exit "$ICS_FD_QA_EXIT_CODE_GITLAB_CURL_TOKEN"
fi

# final check
file $RESULT
if [ "$(file --brief --mime-type $RESULT)" != "application/zip" ]; then
  echo "final check failed: Downloaded file '$RESULT' is not a zip archive! Exiting.."
  exit "$ICS_FD_QA_EXIT_CODE_GITLAB_NOT_ZIP"
fi

echo "done: $RESULT"
echo "===executing script $0 END==="

