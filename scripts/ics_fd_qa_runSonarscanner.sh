#!/bin/bash
# trigger the sonarQube scanner to pick up the reports
# fix certificate:
#
# 1 - Copy the certificate store <java_home>/jre/lib/security/cacerts
#     to a directory to import the sonar certificate only once
# 2 - Get the certificate from sonar server to a file (using openssl)
# 3 - Import the certificate into cacerts file with keytool
# 4 - Export the SONAR_SCANNER_OPTS variable containing the path to the certificate store 
SCANNER=/sonar-scanner-2.7/bin/sonar-scanner
SONAR_SCANNER_FILE="/sonar-scanner-2.7/conf/sonar-scanner.properties"

echo "===executing script $0 START==="
echo "whoami="`whoami`
echo "hostname="`hostname`
echo "pwd="`pwd`
CDIR=`pwd`
XXX=$(readlink -f `which java` | sed "s/java$//g")
cd $XXX/../../jre/lib/security
echo "the java cert store is in "`pwd`
ls -al

cd ${CDIR}
SONAR_SERVER=` cat ${SONAR_SCANNER_FILE} | grep "sonar.host.url" | sed "s/=/ /g" | awk '{printf "%s", $2}' `
echo "sonar server/webpage is at= " ${SONAR_SERVER}


#mkdir -p ~/certificates
#cd ~/certificates
#echo | openssl s_client -connect cvl-sonarqube.cern.ch:443 2>&1 | sed -n -e '/BEGIN\ CERTIFICATE/,/END\ CERTIFICATE/ p' > cvl-sonarqube.cern.ch.crt
#keytool -import -file cvl-sonarqube.cern.ch.crt -alias sonar -keystore cacerts -storepass changeit
#
## slc6sboychen:9000 does not have a certificate
#echo | openssl s_client -connect slc6sboychen:9000 2>&1 | sed -n -e '/BEGIN\ CERTIFICATE/,/END\ CERTIFICATE/ p' > cvl-sonarqube.ics-fd-qa.crt
#keytool -import -file cvl-sonarqube.ics-fd-qa.crt -alias sonar -keystore cacerts -storepass changeit
#export SONAR_SCANNER_OPTS="-Djavax.net.ssl.trustStore=~/certificates/cacerts"

${SCANNER} -v
${SCANNER} -X
echo "===executing script $0 END==="

