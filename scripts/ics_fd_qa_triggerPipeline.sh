#!/bin/csh
#
# trigger the ics-fd-qa image and give it 
# * the artifact URL with the code tree
# * the project name and version as it shall apear in sonarqube as string
echo "===executing script $0 START==="
echo "\n===trigger pipeline ics-fd-qa==="
echo "INPUT_SRC_ARTIFACT= $1"
echo "INPUT_SONARQUBE_NAME= $2"
echo "INPUT_PROJECT_VERSION= $3"
# optional, can be empty, but keep it secret
#echo "INPUT_PROJECT_TOKEN= $4"
echo "INPUT_PROJECT_TOKEN= is kept secret"

if ( $4 == "" ) then
	set DLTOKEN=""
	echo "no INPUT_PROJECT_TOKEN provided"
else
	set DLTOKEN=$4
	echo "an INPUT_PROJECT_TOKEN was provided and is transmitted in the trigger payload"
endif
# keep it masked
# echo "DLTOKEN is ${DLTOKEN}"
#
# we need the pipeline token for that
# https://gitlab.cern.ch/mludwig/ics-fd-qa is project 76083
set TRIGGER_TOKEN=052055a874685b0c0550bc8bc9d55e
set REF=master
set PIPELINE=https://gitlab.cern.ch/api/v4/projects/76083/trigger/pipeline
curl -X POST \
	-F token=$TRIGGER_TOKEN \
	-F ref=$REF \
	-F "variables[INPUT_SRC_ARTIFACT]=$1" \
	-F "variables[INPUT_SONARQUBE_NAME]=$2" \
	-F "variables[INPUT_PROJECT_VERSION]=$3" \
	-F "variables[INPUT_PROJECT_TOKEN]=${DLTOKEN}" \
	$PIPELINE
echo "\n===trigger sent to https://gitlab.cern.ch/mludwig/ics-fd-qa (${REF}) pipeline==="
echo "===executing script $0 END==="

