#!/bin/bash
#
# clang-tidy static code analysis 
#
echo "===executing script $0 START==="
echo "project subdir which contains C++ source= $1"
echo "sonar project name (no whitespaces)= $2"
echo "sonar project version string (no whitespaces)= $3"
#
# we output a sonar-project.properties file which fits the project
if [ -z $1 ] 
then
	echo "please provide a directory with a C++ code tree"
	exit
fi
if [ -z $2 ] 
then
	echo "please provide a sonar project name (no whitespaces)"
	exit
fi
if [ -z $3 ] 
then
	echo "please provide a sonar project version string (no whitespaces)"
	exit
fi
# WIN32 i.e.


# reports are relative to the SRCTREE
REPORT_DIR1=$CACHE_DIR/cppcheck-report
mkdir -p ${REPORT_DIR1}
echo "REPORT_DIR0="${REPORT_DIR0}

#
# source scope: all files are explicitly listed in a filelist
echo "===looking for cpp/h source files, making a list from: "$1"==="
FLIST=$REPORT_DIR0/filelist
rm -rf ${FLIST}; touch ${FLIST}
PATTERN="c cpp cc C CC CPP h hpp H HPP C++ C++ h++ H++ cxx CXX hxx HXX"
for p in $PATTERN; do
	find $1 -name "*.${p}" | grep -v "metadata" >> ${FLIST}
done
echo "wrote list of src files into  ${FLIST}"
echo "===source scope is in filelist "${FLIST}" ==="
cat ${FLIST}
echo "================================="

cppcheck --version
#var=0
echo ${FLIST}
ls -l ${FLIST}
for fn in `cat ${FLIST}`; do
    fname=`basename ${fn}`
    dname=`dirname ${fn}`
    echo "===analysing file "${fn}" , dumping report into ${REPORT_DIR1}/${fname}.xml"
#    echo "===analysing file "${fn}" , dumping report into ${REPORT_DIR1}/${var}.xml"
    cppcheck --xml-version=2 ${fn} 2> ${REPORT_DIR1}/${fname}.xml
#    cppcheck --xml-version=2 ${fn} 2> ${REPORT_DIR1}/${var}.xml
#    ((var++))
done
echo "===done: your reports are in ${REPORT_DIR1}"
echo "===executing script $0 END==="

