#!/bin/bash
#
# prepareSonarqube.sh
#
# produce a project specific sonar-project.properties file in the top level of the src
# sonar scanner gets called from the SRCTREE
# i.e. https://readthedocs.web.cern.ch/display/ICKB/sonarQube+sonar-project.properties
SONAR_PRJ_FILE="sonar-project.properties"
#UNAME=`whoami`
UNAME="BE-ICS-FD QA"
SONAR_SRC=$1
SONAR_PRJ=$2
SONAR_PRJ_VERSION=$3
REPORT_DIR0=$CACHE_DIR/clang-tidy-report
REPORT_DIR1=$CACHE_DIR/cppcheck-report
ISSUE_DIR=$CACHE_DIR/issues
#
echo "===executing script $0 START==="
mkdir -p ${REPORT_DIR0}
touch ${REPORT_DIR0}/info.info
mkdir -p ${REPORT_DIR1}
touch ${REPORT_DIR1}/info.info
#
# according to
#https://github.com/SonarOpenCommunity/sonar-cxx/wiki/Code-checkers
rm -f ${SONAR_PRJ_FILE}; touch ${SONAR_PRJ_FILE}
echo "sonar.projectKey="${SONAR_PRJ}                  >> ${SONAR_PRJ_FILE}
echo "sonar.projectName="${SONAR_PRJ}                 >> ${SONAR_PRJ_FILE}
echo "sonar.projectVersion="${SONAR_PRJ_VERSION}      >> ${SONAR_PRJ_FILE}
echo "sonar.language=c++"                             >> ${SONAR_PRJ_FILE}
echo "sonar.sources="${SONAR_SRC}                     >> ${SONAR_PRJ_FILE}
echo "sonar.cxx.clangtidy.reportPath="${REPORT_DIR0}/*  >> ${SONAR_PRJ_FILE}
#echo "sonar.cxx.clangtidy.customRules= optional"     >> ${SONAR_PRJ_FILE}
#echo "sonar.cxx.clangtidy.charset=     optional"     >> ${SONAR_PRJ_FILE}
echo "sonar.cxx.cppcheck.reportPath="${REPORT_DIR1}/*   >> ${SONAR_PRJ_FILE}
echo "sonar.externalIssuesReportPaths="${ISSUE_DIR}/*   >> ${SONAR_PRJ_FILE}
echo "===sonarQube project.properties in file "${SONAR_PRJ_FILE}"==="
cat  ${SONAR_PRJ_FILE}
echo "=================================="
#
# sonarqube 6.0 with scanner 2.7
# https://binaries.sonarsource.com/Distribution/sonar-scanner-cli/sonar-scanner-2.7.zip
# copy sonar-scanner.properies which lives in /sonar-scanner-2.7/conf (docker) system-wide
SONAR_SCANNER_FILE="scripts/sonar-scanner.properties"
echo "===sonarQube sonar-scanner.properties in file ${SONAR_SCANNER_FILE}==="
cat  ${SONAR_SCANNER_FILE}
echo "=================================="
cp -v ${SONAR_SCANNER_FILE} /sonar-scanner-2.7/conf
echo "===executing script $0 END==="

