#!/bin/bash
# external clang-tidy rules
# cat all rules into one xml to feed into sonarqube as external rules definitions
# rules are named like _*.xml
target="allRulesToAdd.xml"
rm -f ${target}
touch ${target}
echo "<rules>" >> ${target}
dirlist="rulesToAdd_bugprone rulesToAdd_cert rulesToAdd_clang-analyzer rulesToAdd_cppcoreguidelines rulesToAdd_fuchsia rulesToAdd_hicpp rulesToAdd_modernize rulesToAdd_performance rulesToAdd_readability"

for dir in  ${dirlist}; do
	echo "dir= ${dir}"    
	for fn in `ls -1 ${dir}/_*.xml`; do
    	echo "adding rule ${fn}"
    	cat ${fn} | sed "s/<description>/<description>[ics-fd-qa cxx-community clang-tidy external] /g" >> ${target}
	done
done

echo "</rules>" >> ${target}
echo "written all rules into "${target}


