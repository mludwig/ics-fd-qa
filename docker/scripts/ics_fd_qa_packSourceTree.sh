#!/bin/bash
# pack=packSourceTree.sh & trigger
#
# pack the project source tree into an artifact and preserve the structure. 
TARGET=project_sources.tar
echo "===executing script $0 START==="
rm -rf ${TARGET}.*
tar cf ${TARGET} -T /dev/null
ls -l ${TARGET}
#
# source scope: all files are explicitly listed in a filelist
echo "===looking for cpp/h source files, making a list from ./==="
PATTERN="c cpp cc C CC CPP h hpp H HPP C++ C++ h++ H++ cxx CXX hxx HXX"
for p in $PATTERN; do
   find ./ \
	-type f \
	-not -path "*boost*" \
	-name "*.${p}" -exec tar rvf ${TARGET} {} \;
done
echo "OK: packed source tree  into "`pwd`"/${TARGET}"
echo "===executing script $0 END==="


