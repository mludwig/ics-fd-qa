#!/bin/bash
# compare rules of two lists: is rule from $1 found in $2 ?
# $1 and $2 the two lists with rules
for r1 in `cat $1`; do
	#	echo "searching for \"${r1}\" in list $2"
    rule=`echo ${r1} | sed "s/-W/W/g"`
	#	echo "searching for rule ${rule}\" in list $2"
	found=`cat -n $2 | grep "${rule}"`
        if [ -z "${found}" ] 
	then
    	echo "MISSING ${rule}"
    else
		echo "FOUND ${rule} line=${found}"
	fi
done

