#!/bin/bash
rm -fi $1.cleaned.xml
cat $1 |  sed "s/<\/rule>/<\/rule>\n/g" > $1.cleaned.xml
echo "cleaned up xml, new file= "$1.cleaned.xml

newfile=$1.cleaned.xml

rm -fi $1.rules.txt
cat ${newfile} | awk -F "<key>" '{printf "%s\n", $2}' | sed "s/<\/key>/ /g" | awk '{printf "%s\n", $1}' > $1.rules.txt
echo "written rules to "$1.rules.txt
