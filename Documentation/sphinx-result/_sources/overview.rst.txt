========
Overview
========

This is a user manual "for developers" for the `ICS-FD-QA project`_ (edms pptx). The objectives of this project are:

	- provide integrated code quality assessment and metrics for a wide range of SW code basis
	- standardize analysis method but ignore individual coding syles 
	- perform high quality "code scans" to find "fishy" code automatically
	- avoid false-positive reports to keep noise down (i.e. dont't report minor style issues)
	- implicitly accept some false-negative misses
	- user friendly graphical output, with overviews and history as possible
	- quick learning curve, easy to integrate into workflow
	- provide clear code fixes if possible for standard cases
	- have minimal impact on existing build chains
	- respect project needs: configuration per project
	- respect copyright needs: encapsulate toolchain and reports behind authentication
	- scale if needed to new/heavy usage
	- easily maintainable, virtualisation, compatible (you want it all)
	
The tools used are:
	- gitlab CI pipelines, triggers, linux, cern-cc7
	- docker images
	- llvm series of compilers and analyzers, orthogonal to gcc
	- cppcheck
	- sonarqube / scanner as graphical reporting tool
	- a couple of scripts and standard unix tools
	- brain power and patience
	
The initial focus is on C++ code analysis.
	  
.. image:: ics-fd-qa-execution.jpg
	  
The project pipeline (green) is started (1) by i.e. a source push to gitlab. An extra stage (ics-fd-qa) and job (pack) 
have to be integrated into the pipeline right after the building stages, by remotely including a template yml (2). 
When all project sources are packed the ics fd qa pipeline (violet) is triggered using a payload (3). If a token was specified in the
payload (4) it is used (4a), if not, the service account token from icsfdqa is used (4b), and in this case the service account
must be a developer in the gitlab project. The ics fd qa pipeline can then download the sources (5) and run the static 
code analysis (6). All existing rules are used at that stage against the code and issues are recorded. When the scanner 
is invoked first the configured project rules are downloaded (7) and then issues are filtered using these project rules 
and uploaded. All existing rules must be configured in the sonarqube CXX plugin beforehand in order to have a free choice 
of rules, according to the project. The sonarqube instance finally publishes the issues in a Web API (9) and the developer
can revise the code using the interactive API for further support (10).

	  
.. _ICS-FD-QA project:	  https://edms.cern.ch/file/2133877/1/ics-fd-qa.01.pptx