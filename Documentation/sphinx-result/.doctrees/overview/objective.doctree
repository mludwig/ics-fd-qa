���      �docutils.nodes��document���)��}�(�	rawsource�� ��children�]�h �section���)��}�(hhh]�(h �title���)��}�(h�CANX overview and objective�h]�h �Text����CANX overview and objective�����}�(hh�parent�hhh�source�N�line�Nuba�
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]�u�tagname�hhhhhh�n/home/mludwig/projects/CanModule/CanModule#master/CanModule/Documentation/sphinx-source/overview/objective.rst�hKubh �	paragraph���)��}�(hX�  CANX is a multithreaded "CAN message player and recorder" which uses CanModule and therefore interacts
with all CAN bridges which are supported by CanModule. CANX is available for all supported target platforms,
notably cc7, windows 2016 server and windows 10 enterprise. The sources and cross-system build chain are
available ((c) CERN). CANX is "a client" to CanModule, much in the same way as any OPC-UA server would be.�h]�hX�  CANX is a multithreaded “CAN message player and recorder” which uses CanModule and therefore interacts
with all CAN bridges which are supported by CanModule. CANX is available for all supported target platforms,
notably cc7, windows 2016 server and windows 10 enterprise. The sources and cross-system build chain are
available ((c) CERN). CANX is “a client” to CanModule, much in the same way as any OPC-UA server would be.�����}�(hh/hh-hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKhhhhubh,)��}�(hXr  CanModule is a software abstraction layer, written in C++, to simplify integration
of CAN bridges from different vendors into cmake (C++) projects needing CAN connectivity
for windows and linux. A CAN bridge is - usually - an external module which is connected
on one side to a computer or network and offers CAN ports on the other side where CAN buses
can be connected.�h]�hXr  CanModule is a software abstraction layer, written in C++, to simplify integration
of CAN bridges from different vendors into cmake (C++) projects needing CAN connectivity
for windows and linux. A CAN bridge is - usually - an external module which is connected
on one side to a computer or network and offers CAN ports on the other side where CAN buses
can be connected.�����}�(hh=hh;hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK
hhhhubh,)��}�(hX  CERN CentOs7 cc7 and Windows 2016 Server are supported.
CAN bridges from Vendors Anagate, Systec and Peak are supported.
CanModule can be source-cloned from gitlab.cern.ch and integrated into a cmake build chain.
For other build chains the binaries and libs can be made available.�h]�hX  CERN CentOs7 cc7 and Windows 2016 Server are supported.
CAN bridges from Vendors Anagate, Systec and Peak are supported.
CanModule can be source-cloned from gitlab.cern.ch and integrated into a cmake build chain.
For other build chains the binaries and libs can be made available.�����}�(hhKhhIhhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKhhhhubeh}�(h]��canx-overview-and-objective�ah!]�h#]��canx overview and objective�ah%]�h']�uh)h	hhhhhh*hKubah}�(h]�h!]�h#]�h%]�h']��source�h*uh)h�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(hN�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�h��error_encoding��UTF-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�h*�_destination�N�_config_files�]��pep_references�N�pep_base_url�� https://www.python.org/dev/peps/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��https://tools.ietf.org/html/��	tab_width�K�trim_footnote_reference_space���file_insertion_enabled���raw_enabled�K�syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���embed_stylesheet���cloak_email_addresses���env�Nub�reporter�N�indirect_targets�]��substitution_defs�}��substitution_names�}��refnames�}��refids�}��nameids�}�h\hYs�	nametypes�}�h\Nsh}�hYhs�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �id_start�K�parse_messages�]��transform_messages�]��transformer�N�
decoration�Nhhub.