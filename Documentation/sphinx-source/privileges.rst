==========
privileges
==========

Several authentications/privileges are needed for making all stages work:
	
	1. trigger privilege (=push) from project-pipeline to QA-pipeline
	-----------------------------------------------------------------
	
	The QA-pipeline stems from the `ics-fd-qa-project`_ . A CI/CD pipeline trigger token
	is defined and integrated (hardcoded) into the trigger script. The trigger script 
	is integrated into the client docker image at `ics-fd-qa-repo`_ . Therefore there is nothing
	to be done explicitly
	
	2. download privilege (=pull) from QA-pipeline to project-pipeline source code artifact
	---------------------------------------------------------------------------------------
	
    A gitlab token is needed to download the project's source code tree for analysis. Nevertheless
    access rights might be restricted for legal reasons. See gitlab project settings for details on
    tokens.
    
    - the project wants to provide it's own download token, which must be an "API" token linked to the maintaining user or a service account used by the project. Commonly, an API token from a service account is used, and in order to limit the service account's rights to the project the service account is a "gitlab developer" member only (NOT: maintainer). The token is passed in an environment variable which must be named ICS_FD_QA_READ. An API token from the user should NOT be used (it would give gitlab "maintainer" rights away through the token) but it would also work. The specified token is passed as trigger payload but kept invisible.
    
    - the project does not provide it's own download token, no environment var is needed. In this case the ics-fd-qa analyser will use it's own API token to download: the service account "icsfdqa" is used for this. For this to work the icsfdqa service account must be added as a "developer" to the gitlab project. No token is passed as payload, and the API token finally used is also kept invisible.  
    
	
	3. result-publishing to sonar privilege
	---------------------------------------
	
	The QA-pipeline has to push the results to the sonar Web API using the sonar-scanner. 
	Privileges are configured in the sonar-scanner CLI configuration, and everything is 
	integrated into the analysis docker image: nothing to do either.  
	
	
.. _ics-fd-qa-repo: https://gitlab-registry.cern.ch/mludwig/ics-fd-qa
.. _ics-fd-qa-project: https://gitlab.cern.ch/mludwig/ics-fd-qa
	