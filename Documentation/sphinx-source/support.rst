=======
Support
=======

Support for ICS-FD-QA is given to BE-ICS group members. Please contact me directly for now:

* You can send me an `Email`_ or call me 163095 or visit me for a coffee.

EOS account
-----------

service account: icsfdqa  ics.fd.qa@cern.ch passwd:Clang1cs
EOS project documentation root folder: ics-fd-qa
web content root: lxplus:/eos/project/i/ics-fd-qa/www
egroup: cernbox-projects-ics-fd-qa-admins
egroup: cernbox-projects-ics-fd-qa-readers
egroup: cernbox-projects-ics-fd-qa-writers

the egroup be-dep-ics-fd is a member of cernbox-projects-ics-fd-qa-writers
the egroup be-dep-ics is a member of cernbox-projects-ics-fd-qa-readers


raw file access
---------------

via ssh/scp lxplus
via browser https://cernbox.cern.ch/index.php/apps/files/?dir=/__myprojects/ics-fd-qa/www&


documentation access for users
------------------------------
this EOS project space has an official website running on top, via webservices:
http://cern.ch/ics-fd-qa


.. _Email: mailto:michael.ludwig@cern.ch?subject=ics-fd-qa_issue_found&body=Hello Michael,