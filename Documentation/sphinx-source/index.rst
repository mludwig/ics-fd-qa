.. _contents:

Quality Assurance for ICS-FD software
=====================================

We do static code analysis on C++ code and publish the results on a ergonomical website to help the developer to review any issues. An abstract syntax tree based approach using the llvm/clang compiler tools is used as an external tool, along with several other analysers.  

.. toctree::
   :maxdepth: 3

   quickstart
   overview
   configuration
   cpp/docker
   cpp/clangtidy
   execution
   privileges
   pipelines
   support
   
alphabetical index
------------------

* :ref:`genindex`

.. * :ref:`search`
.. * :ref:`modindex`


