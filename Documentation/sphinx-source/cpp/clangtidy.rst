clang-tidy and sonar
--------------------

In order to benefit from the full power and flexibility of clang-tidy as an 
external code analyzer it is configured to run a maximum number of useful
tests on the code (file .clang-tidy). The configuration of clang-tidy is therefore "big" and the
runs will take some amount of CPU time. A potentially big amount of issues is 
generated (easily many MBytes and several 10k issues).

Each issue contains information about:
	- the rule which triggered the issue
	- filename, line, of offending code
	- a description or message of the "complaint", hopefully containing an URL for further information
	- an issue severity, tag(s) and code management parameters
	
	
Example of an issue:

.. code-block:: c++

		const         &
		./include/API.h:153:52: warning: the parameter 'h' is copied for each invocation but only used as a const reference; consider making it a const reference [performance-unnecessary-value-param]
        static uint16_t getPropAuxDecimal( HAL_PROPERTY_t h ){ return( h.attributeProperties.decimal ); }
                                                          ^	
	
The sonar API has a certain number of rules defined as "known" (sonar-rules-set), 
and it will only treat issues which stem from known rules. Typically the number
and scope of clang-tidy rules is much bigger and overlaps only partly 
with sonar-rules-set. Therefore the clang-tidy rules which are "missing"
have to be added to the sonar-rules-set, by importing a list of rule-headers as an xml:

.. code-block:: c++

	<rules>
		<rule>
			<key>bugprone-argument-comment</key>
			<name>bugprone-argument-comment</name>
 			<description>[ics-fd-qa cxx-community clang-tidy external] Checks that argument comments match parameter names.
			The check understands argument comments in the form /*parameter_name=*/ that are placed right before the argument.
			https://clang.llvm.org/extra/clang-tidy/checks/bugprone-argument-comment.html
			</description>
    		<tag>llvm</tag>
    		<tag>pitfall</tag>
    		<debtRemediationFunction>CONSTANT_ISSUE</debtRemediationFunction>
    		<remediationFunctionBaseEffort>10min</remediationFunctionBaseEffort>
		</rule>
		<rule>
		...
		</rule>
	</rules>
  
These rules are imported and registered through the sonar cxx::community plugin.
The sonar-scanner CLI is then configured in a standard way to pick up all issues 
in a specific directory and transmit to sonar. Only the issues which are activated
in the corresponding sonar-quality-profile are transmitted. 

Therefore - by carefully and comfortably managing the sonar-quality-profile which corresponds to the code
in question in the sonar API - an interesting and useful set of rules (ics-fd-qa C++) can be created 
from a wide range of available rules.  


 