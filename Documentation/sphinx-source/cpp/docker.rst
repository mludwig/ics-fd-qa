docker
------

The docker image for C++ static code analysis contains up-to-date versions (Oct 2019) of 
	- `llvm`_/clang/clang-tidy 8.0.1 (see `doc`_ for build instructions)
	- `cmake`_ 3.15.2
	- `sonar-scanner`_ CLI 2.7 (compatible with `sonar`_ API 6.0)
	
and it is based on cc7.

The image is built in a classical way using the docker system and delivered into the `gitlab`_ container registry:

.. code-block:: c++

	FROM gitlab-registry.cern.ch/linuxsupport/cc7-base
	(...)
	ADD tgz/cmake-3.15.2.tar.xz /
	RUN cd cmake-3.15.2 && ./bootstrap && make -j 5 && make install
	ADD tgz/llvm-8.0.1.src.tar.xz /
	ADD tgz/cfe-8.0.1.src.tar.xz /llvm-8.0.1.src/tools
	ADD tgz/clang-tools-extra-8.0.1.src.tar.xz /llvm-8.0.1.src/tools/cfe-8.0.1.src/tools
	RUN ln -s  /llvm-8.0.1.src/tools/cfe-8.0.1.src/tools/clang-tools-extra-8.0.1.src /llvm-8.0.1.src/tools/cfe-8.0.1.src/tools/extra
	RUN cd llvm-8.0.1.src && mkdir build && cd build && /cmake-3.15.2/bin/cmake -DLLVM_TEMPORARILY_ALLOW_OLD_TOOLCHAIN=y -DCMAKE_BUILD_TYPE=Release .. && make -j 5 && make install && make clean
	ADD tgz/sonar-scanner-2.7.tar.xz /

The image does NOT call any entry-point script, since it is triggered through the projects' gitlab pipeline 
using a gitlab-trigger with payload. The gitlab system basically "occupies the entry-point for the image",
therefore no specific entry-point is possible.

.. _sonar-scanner: https://github.com/SonarSource/sonar-scanner-cli/releases/tag/2.7
.. _sonar: https://binaries.sonarsource.com/Distribution/sonarqube/sonarqube-6.0.zip
.. _cmake: https://cmake.org/download/
.. _llvm: http://releases.llvm.org/
.. _doc: https://readthedocs.web.cern.ch/pages/viewpage.action?spaceKey=ICKB&title=llvm%3A+clang-tidy
.. _gitlab: gitlab-registry.cern.ch/mludwig/ics-fd-qa
