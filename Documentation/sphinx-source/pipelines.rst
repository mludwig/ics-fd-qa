=========
pipelines
=========

The project-pipeline (with the source code to analyse) has an extra stage "ics-fd-qa" with job "pack". This job is  
executed immediately after the build (when all sources are available), but generally the "ics-fd-qa" stage
is not the last stage in the project-pipeline: there might be documentation and unit-tests later.

Nevertheless the artifact containing the source code is only available for download after the completion of the 
project-pipeline. The QA-pipeline has therefore a strong tendency "to be too early": it is triggered already before
the project-pipeline has finished.

Either the call to the "pack" and "trigger" scripts are split into two stages in the project-pipeline, so that 
the "pack" occurs after the "build", and the "trigger" occurs as the last stage. Or - in order to minimize 
the impact on the project-pipeline - the "artifact downloading" waits and tries many times
to download the artifact before it exits with an error (after ~1 hour).  
	
	