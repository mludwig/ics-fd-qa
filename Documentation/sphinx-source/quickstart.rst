==========
Quickstart
==========


You can refer to the online documentation:
https://readthedocs.web.cern.ch/display/ICKB/ics-fd-qa?src=contextnavpagetreemode

Or follow the 3 steps:

1. add stage "ics-fd-qa"
------------------------
 
to your project's .gitlab-ci.yml file (edit .gitlab-ci.yml). This should be
immediately AFTER the build, when all sources are available, also the generated ones.

.. code-block:: c++

          stages: 
             - ics-fd-qa

2. include the remote job from ics-fd-qa
----------------------------------------

as template in project's .gitlab-ci.yml file (edit .gitlab-ci.yml):
 
.. code-block:: c++

		  include:
 		     - remote: 'https://gitlab.cern.ch/mludwig/ics-fd-qa/raw/master/.ics-fd-qa-template.yml'

3. provide a download token - two possibilities
-----------------------------------------------

    a. provide your own project token as a payload
    - the token needs API privileges, and the token value must be copied into the environment variable ICS_FD_QA_READ
    - copy your API token value to your clipboard, then paste it into the env variable
    - we need to pass the token's value through the environment in order to standardize ics-fd-qa properly
    - you should not use and API token from your primary account, but from a service account instead, and restrict that service account to "gitlab developer" in gitlab's Settings->Members

    b. actually provide no token (leave empty)
    - you need to add the service account icsfdqa to "gitlab developer" in gitlab's Settings->Members
    - the API token from the service account "icsfdqa" is used


Note: we need "API" tokens with full rights from an account, "download" tokens which are project related do not have sufficient privileges


