=========
execution
=========

project-pipeline/project="the project which has the sources to be analysed"
QA-pipeline/project="the code analyzer and publisher"

The QA-pipeline is almost entirely separated from the project's build pipeline. Each time when new code is 
delivered to the project, the project pipeline runs. Right after the (successful) build stage all the needed
sources are available inside the build container. The (C++) sources can roughly be classified into 4 categories:

	#. specific project sources 
	#. pulled in sources from external projects ( via gitlab clones etc)
	#. generated sources
	#. headers and libraries from the system and 3rd party origins
	
A "source code packing stage" is added to the project pipeline (project .gitlab-ci.yml) which does 

	#. download scripts "pack" and "trigger" (from nexus) 
	#. pack the source code in an artifact (script pack)
	#. trigger the QA-pipeline and pass as trigger-payload (script trigger):
		- the URL of the source code artifact
		- sonar project name (which is the project's name)
		- sonar project version (which is the project's name)
		
	only the "master" branch of the QA-project is triggered, as specified in the trigger payload as a parameter.
	
the project .gitlab-ci.yml needs to add:
	
.. code-block:: c++

	stages:
		- ics-fd-qa

and also


.. code-block:: c++

	# pack the project source tree into an artifact and trigger the ics-fd-qa pipeline.
	# The script must be run inside the build container so that all sources are available. 
	# The script itself is pulled from nexus to avoid that the build image has to provide it.
	pack:
		stage: ics-fd-qa
		script:
			- echo "downloading ics-fd-qa scripts from nexus"
			- wget https://repository.cern.ch/nexus/service/local/repositories/cern-venus/content/ics-fd-qa/ics_fd_qa_packSourceTree.sh
			- chmod u+x ics_fd_qa_packSourceTree.sh
			- wget https://repository.cern.ch/nexus/service/local/repositories/cern-venus/content/ics-fd-qa/ics_fd_qa_triggerPipeline.sh
			- chmod u+x ics_fd_qa_triggerPipeline.sh
			#
			# pack sources
			- cd $CI_PROJECT_DIR && ./ics_fd_qa_packSourceTree.sh $CI_PROJECT_DIR
			#
			# trigger the ics-fd-qa pipeline and give it  
			# * the artifact URL with the code tree
			# * the project name and version as it shall apear in sonarqube as string (take CI_PROJECT_NAME)
			- URL="$CI_PROJECT_URL/-/jobs/$CI_JOB_ID/artifacts/download"
			- VERSION=${CI_COMMIT_REF_SLUG}"-"${CI_COMMIT_SHORT_SHA}
			- cd $CI_PROJECT_DIR && ./ics_fd_qa_triggerPipeline.sh $URL $CI_PROJECT_NAME $VERSION 
		artifacts:
			name: project_sources
			paths:    
				- project_sources.tar
		


.. _QA-gitlab-project:   https://gitlab.cern.ch/mludwig/ics-fd-qa

  